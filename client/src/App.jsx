import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

// Custome Components
import { NavBar } from "./Components";
import { Home, Login, Register } from "./Pages";

const App = () => {
  const isLoggedIn = sessionStorage.getItem("loggedIn");

  return (
    <>
      <Router>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={isLoggedIn ? <Home /> : <Login />} />
          <Route
            path="/register"
            element={isLoggedIn ? <Home /> : <Register />}
          />
        </Routes>
      </Router>
    </>
  );
};

export default App;
