import { Link, useNavigate } from "react-router-dom";

/* "antd" desgin components starts here*/
import {
  LoginOutlined,
  CalendarOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { Button } from "antd";
/* "antd" desgin components ends here*/

const NavBar = () => {
  const Navigate = useNavigate();
  const isLoggedIn = sessionStorage.getItem("loggedIn");

  const handleLogout = () => {
    sessionStorage.removeItem("userToken");
    sessionStorage.removeItem("loggedIn");
    Navigate("/login");
    window.location.reload();
  };

  return (
    <div className="shadow-md w-full relative top-0 left-0">
      <div className="flex px-2 md:flex items-center justify-between bg-white py-5 md:px-6">
        <div className="flex items-center">
          <Link
            to={"/"}
            className="font-bold text-2xl cursor-pointer flex items-center font-[Poppins] text-gray-800"
          >
            <CalendarOutlined className="px-2 md:px-1 text-blue-600" />
            Calendar
          </Link>
        </div>

        <section className="flex items-center gap-4">
          {isLoggedIn ? (
            <Button
              type="primary"
              shape="round"
              icon={<LogoutOutlined />}
              size={"middle"}
              onClick={handleLogout}
            >
              Logout
            </Button>
          ) : (
            <>
              <Link to={"/login"}>
                <Button
                  type="primary"
                  shape="round"
                  icon={<LoginOutlined />}
                  size={"middle"}
                >
                  Login
                </Button>
              </Link>
              <Link to={"/register"}>
                <Button shape="round">Register</Button>
              </Link>
            </>
          )}
        </section>
      </div>
    </div>
  );
};

export default NavBar;
