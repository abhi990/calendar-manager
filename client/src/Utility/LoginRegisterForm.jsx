import { message } from "antd";

// Calling APIs to use form Services folder
import { loginFunction, registerFunction } from "../Services/APIs";

const LoginRegisterForm = async (
  values,
  setSpinner,
  form,
  type,
  navigateFunction
) => {
  setSpinner(true);
  try {
    // running different API for login and register
    const response =
      type === "login"
        ? await loginFunction(values)
        : await registerFunction(values);

    if (response.status === 200) {
      message.success(response.data.message);
      if (type === "login") {
        sessionStorage.setItem("userToken", response.data.userToken);
        sessionStorage.setItem("loggedIn", true);
        // Delay made to show the "message.success..." and then to  navigate
        setTimeout(() => {
          navigateFunction("/");
          window.location.reload();
        }, 2000); // Delay of 2 seconds
      } else {
        // Delay made to show the "message.success..." and then to  navigate
        setTimeout(() => {
          navigateFunction("/login");
          window.location.reload();
        }, 2000); // Delay of 2 seconds
      }
    } else {
      message.error(response.response.data.message);
    }
  } catch (error) {
    if (error.response) {
      message.error(error.response.data.message);
    } else {
      message.error("An error occurred. Please try again later.");
    }
  } finally {
    setSpinner(false);
    form.resetFields();
  }
};

export default LoginRegisterForm;
