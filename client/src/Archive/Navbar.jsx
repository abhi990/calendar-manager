/*import { useState } from "react";
import { FiMenu } from "react-icons/fi";
import { IoCloseSharp } from "react-icons/io5";
*/
import { LoginOutlined } from "@ant-design/icons";
import { Button } from "antd";

const NavBar = () => {
  //let [open, setOpen] = useState(false);
  return (
    <div className="shadow-md w-full fixed top-0 left-0">
      <div className="flex px-8 md:flex items-center justify-between bg-white py-5 md:px-10">
        <div className="flex items-center gap-3">
          <div className="font-bold text-2xl cursor-pointer flex items-center font-[Poppins] text-gray-800">
            NavBar
          </div>
        </div>

        <section className="flex items-center gap-4">
          <Button
            type="primary"
            shape="round"
            icon={<LoginOutlined />}
            size={"middle"}
          >
            Login
          </Button>
          <Button shape="round">Register</Button>
        </section>
        {/*
        {open ? (
              <IoCloseSharp
                onClick={() => setOpen(false)}
                className="text-3xl cursor-pointer lg:hidden mr-2"
              />
            ) : (
              <FiMenu
                onClick={() => setOpen(true)}
                className="text-3xl cursor-pointer lg:hidden mr-2"
              />
            )}*/}
      </div>
    </div>
  );
};

export default NavBar;
