import { Calendar } from "antd";

const CalendarCmp = () => {
  const onPanelChange = (value, mode) => {
    console.log("Current active month is: ", new Date(value).getMonth());
    console.log(value.format("YYYY-MM-DD"), mode);
  };
  return (
    <Calendar
      onPanelChange={onPanelChange}
      onSelect={(date) => {
        const selectedDate = new Date(date).getDate();
        const selectedMonth = new Date(date).getMonth();
        const selectedYear = new Date(date).getFullYear();
        console.log(
          "Selected date: ",
          selectedDate,
          " of month: ",
          selectedMonth,
          " of year: ",
          selectedYear
        );
      }}
    />
  );
};
export default CalendarCmp;

/*
disabledDate={(date) => {
        if (new Date(date).getDate() > 28) {
          return true;
        } else {
          return false;
        }
      }}
      dateCellRender={(date) => {
        if (new Date(date).getDate() === new Date().getDate()) {
          return (
            <>
              Learning Date
              <br />
              Practicing date
            </>
          );
        }
      }}
      monthCellRender={(date) => {
        if (new Date(date).getMonth() === new Date().getMonth()) {
          return (
            <>
              Learning Date
              <br />
              Practicing date
            </>
          );
        }
      }}

      */
