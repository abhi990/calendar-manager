import { useState } from "react";
import { Calendar, Modal } from "antd";

const Home = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedDateDetails, setSelectedDateDetails] = useState({
    date: null,
    month: null,
    year: null,
  });

  const onPanelChange = (value, mode) => {
    console.log("Current active month is: ", value.month());
    console.log(value.format("YYYY-MM-DD"), mode);
  };

  const handleSelect = (date) => {
    const selectedDate = date.date();
    const selectedMonth = date.month();
    const selectedYear = date.year();
    setSelectedDateDetails({
      date: selectedDate,
      month: selectedMonth + 1, // Month is 0-indexed in moment.js
      year: selectedYear,
    });
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Calendar onPanelChange={onPanelChange} onSelect={handleSelect} />
      <Modal
        title="Selected Date"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>Selected date: {selectedDateDetails.date}</p>
        <p>Selected month: {selectedDateDetails.month}</p>
        <p>Selected year: {selectedDateDetails.year}</p>
      </Modal>
    </>
  );
};

export default Home;
