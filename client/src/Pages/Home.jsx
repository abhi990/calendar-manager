import { useEffect, useState } from "react";
import moment from "moment";

// ant-design components
import {
  Calendar,
  Modal,
  List,
  Card,
  Button,
  Form,
  Input,
  TimePicker,
  message,
} from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";

// Calling all APIs to use
import {
  addEventFunction,
  allEventsFunction,
  deleteEventFunction,
  updateEventFunction,
} from "../Services/APIs";

const Home = () => {
  const [tasks, setTasks] = useState([]);
  const [filteredTasks, setFilteredTasks] = useState([]);
  const [selectedDateDetails, setSelectedDateDetails] = useState({
    date: null,
    month: null,
    year: null,
  });

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isTaskModalVisible, setIsTaskModalVisible] = useState(false);
  const [currentTask, setCurrentTask] = useState(null);

  /* Getting user's all tasks data starts here */
  const [currMonth, setCurrMonth] = useState(new Date().getMonth() + 1);
  const [currYear, setCurrYear] = useState(new Date().getFullYear());

  // whenever userToken, isloggedIn, current month or year changes, this useEffect is triggered
  const isLoggedIn = sessionStorage.getItem("loggedIn");
  const userToken = sessionStorage.getItem("userToken");
  useEffect(() => {
    const fetchData = async () => {
      try {
        const getAllUserEvents = await allEventsFunction({
          token: userToken,
          month: currMonth,
          year: currYear,
        });

        console.log(getAllUserEvents.data.events);
        if (getAllUserEvents.status === 200) {
          setTasks(getAllUserEvents.data.events);
        }
      } catch (error) {
        console.error("Error fetching user data:", error);
      }
    };

    if (isLoggedIn) {
      fetchData();
    }
  }, [isLoggedIn, userToken, currMonth, currYear]);
  /* Getting user's all tasks data ends here */

  /* When a user selects a new date on a certain month and year, this useEffect is triggered */
  useEffect(() => {
    if (
      selectedDateDetails.date &&
      selectedDateDetails.month &&
      selectedDateDetails.year
    ) {
      const filteredTasksForDate = tasks.filter(
        (task) =>
          task.date === selectedDateDetails.date &&
          task.month === selectedDateDetails.month &&
          task.year === selectedDateDetails.year
      );
      setFilteredTasks(filteredTasksForDate);
    }
  }, [selectedDateDetails, tasks]);

  // Updating month and year to get new tasks for the current month from the backend
  const onPanelChange = (value) => {
    setCurrMonth(value.month() + 1);
    setCurrYear(value.year());
  };

  // Handling current selected date
  const handleSelect = (date) => {
    if (isLoggedIn) {
      setSelectedDateDetails({
        date: date.date(),
        month: date.month() + 1,
        year: date.year(),
      });
      setIsModalVisible(true);
    } else {
      message.error("Login to add task");
    }
  };

  /* Closing modals when the cross ("X") button is clicked */
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  /* Closing add task modals when the cross ("X") button is clicked */
  const handleTaskModalCancel = () => {
    setIsTaskModalVisible(false);
    setIsEditing(false);
    setCurrentTask(null);
  };

  /* Deleting a task for a user based on user token and task id */
  const handleDelete = async (taskId) => {
    try {
      const response = await deleteEventFunction({
        token: userToken,
        eventId: taskId,
      });
      // console.log(response);
      if (response.status === 200) {
        message.success(response.data.message);
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.error("Error deleting event:", error);
      message.error("Failed to delete event. Please try again.");
    }
  };

  /* Handling Add new task and Update existing task starts here*/
  const [isEditing, setIsEditing] = useState(false);
  // Handling edit task
  const handleEdit = (taskId) => {
    const taskToEdit = tasks.find((task) => task._id === taskId);
    setCurrentTask(taskToEdit);
    setIsEditing(true);
    setIsTaskModalVisible(true);
  };

  // Opening add task modal
  const handleAddTask = () => {
    setIsTaskModalVisible(true);
    setIsEditing(false);
    setCurrentTask(null);
  };

  // Handling add new task or updating a task
  const handleFormSubmit = async (values) => {
    const newTask = {
      token: userToken,
      title: values.title,
      description: values.description,
      startTime: moment(values.startTime).format("HH:mm"),
      endTime: moment(values.endTime).format("HH:mm"),
      date: selectedDateDetails.date,
      month: selectedDateDetails.month,
      year: selectedDateDetails.year,
    };

    // console.log(newTask);
    try {
      let response;
      if (isEditing && currentTask) {
        // Update existing task
        response = await updateEventFunction({
          token: userToken,
          eventId: currentTask._id,
          newTitle: newTask.title,
          newDescription: newTask.description,
          newStartTime: newTask.startTime,
          newEndTime: newTask.endTime,
        });
      } else {
        // Add new task
        response = await addEventFunction(newTask);
      }

      console.log(response);
      if (response.status === 200) {
        message.success(response.data.message);
        setTimeout(() => {
          setIsTaskModalVisible(false);
          window.location.reload();
        }, 2000);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.error("Error adding/updating event:", error);
      message.error("Failed to add/update event. Please try again.");
    }
  };
  /* Handling Add new task and Update existing task ends here*/

  // Displaying the task on the calendar for specific dates and specific user
  const dateCellRender = (date) => {
    const currentDate = date.date();
    const currentMonth = date.month() + 1;
    const currentYear = date.year();
    const tasksForDate = tasks.filter(
      (task) =>
        task.date === currentDate &&
        task.month === currentMonth &&
        task.year === currentYear
    );

    return tasksForDate.length ? (
      <ul>
        {tasksForDate.map((task) => (
          <li key={task.id} className="text-blue-500">
            {task.title}
          </li>
        ))}
      </ul>
    ) : null;
  };

  return (
    <>
      <Calendar
        onPanelChange={onPanelChange}
        onSelect={handleSelect}
        dateCellRender={dateCellRender}
      />
      <Modal
        title={`Tasks for ${selectedDateDetails.date}-${selectedDateDetails.month}-${selectedDateDetails.year}`}
        visible={isModalVisible}
        onCancel={handleCancel}
        footer={[
          <Button key="addTask" type="primary" onClick={handleAddTask}>
            Add Task
          </Button>,
        ]}
        width={600}
      >
        <List
          itemLayout="vertical"
          dataSource={filteredTasks}
          renderItem={(item) => (
            <List.Item
              actions={[
                <Button
                  key={item._id}
                  type="link"
                  icon={<EditOutlined />}
                  onClick={() => handleEdit(item._id)}
                />,
                <Button
                  key={item._id}
                  type="link"
                  icon={<DeleteOutlined />}
                  onClick={() => handleDelete(item._id)}
                  className="text-red-500"
                />,
              ]}
            >
              <Card>
                <h3>{item.title}</h3>
                <p>{item.description}</p>
                <p>
                  <strong>Start Time:</strong> {item.startTime}
                </p>
                <p>
                  <strong>End Time:</strong> {item.endTime}
                </p>
              </Card>
            </List.Item>
          )}
        />
      </Modal>

      <Modal
        title={isEditing ? "Edit Task" : "Add New Task"}
        visible={isTaskModalVisible}
        onCancel={handleTaskModalCancel}
        footer={null}
      >
        <Form
          layout="vertical"
          onFinish={handleFormSubmit}
          initialValues={{
            title: currentTask ? currentTask.title : "",
            description: currentTask ? currentTask.description : "",
            startTime: currentTask
              ? moment(currentTask.startTime, "HH:mm")
              : moment("00:00", "HH:mm"),
            endTime: currentTask
              ? moment(currentTask.endTime, "HH:mm")
              : moment("00:00", "HH:mm"),
          }}
        >
          <Form.Item
            name="title"
            label="Title"
            rules={[{ required: true, message: "Please input the title!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="description"
            label="Description"
            rules={[
              { required: true, message: "Please input the description!" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="startTime"
            label="Start Time"
            rules={[
              { required: true, message: "Please input the start time!" },
            ]}
          >
            <TimePicker format="HH:mm" />
          </Form.Item>
          <Form.Item
            name="endTime"
            label="End Time"
            rules={[{ required: true, message: "Please input the end time!" }]}
          >
            <TimePicker format="HH:mm" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit">
              {isEditing ? "Update Task" : "Add Task"}
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default Home;
