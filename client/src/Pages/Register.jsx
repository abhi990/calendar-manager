import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

/* "antd" design component starts here */
import {
  LockOutlined,
  UserOutlined,
  LoadingOutlined,
  MailOutlined,
} from "@ant-design/icons";
import { Button, Form, Input } from "antd";
/* "antd" design component ends here */

// Importing utility function to handle register form submit
import { LoginRegisterForm } from "../Utility";

const Register = () => {
  const Navigate = useNavigate();

  /* Register form function starts here */
  const [form] = Form.useForm();
  const [spinner, setSpinner] = useState(false);

  const handleRegisterFormSubmit = (values) => {
    LoginRegisterForm(values, setSpinner, form, "register", Navigate);
  };
  /* Register form function ends here */

  return (
    <div className="flex justify-center items-center my-3 h-100">
      <Form
        form={form}
        name="signup"
        className="signup-form p-6 border rounded-lg shadow-lg bg-white"
        initialValues={{
          remember: true,
        }}
        style={{
          minWidth: 350,
        }}
        onFinish={handleRegisterFormSubmit}
      >
        <Form.Item
          name="name"
          rules={[
            {
              required: true,
              message: "Please input your Name!",
            },
          ]}
          className="mb-4"
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Name"
            className="p-2 border rounded-md"
          />
        </Form.Item>
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your Email!",
            },
          ]}
          className="mb-4"
        >
          <Input
            prefix={<MailOutlined className="site-form-item-icon" />}
            placeholder="Email"
            className="p-2 border rounded-md"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your Password!",
            },
          ]}
          className="mb-4"
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
            className="p-2 border rounded-md"
          />
        </Form.Item>
        <Form.Item
          name="confirm"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Please confirm your Password!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error("The two passwords do not match!")
                );
              },
            }),
          ]}
          className="mb-4"
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Confirm Password"
            className="p-2 border rounded-md"
          />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="signup-form-button px-4 w-full"
          >
            Register
            {spinner ? <LoadingOutlined /> : ""}
          </Button>
          <div className="mt-2 text-center">
            Or{" "}
            <Link to={"/login"} className="text-blue-500">
              log in now!
            </Link>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Register;
