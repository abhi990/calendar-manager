import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

/* "antd" design component starts here */
import { LockOutlined, UserOutlined, LoadingOutlined } from "@ant-design/icons";
import { Button, Form, Input } from "antd";
/* "antd" design component ends here */

// Importing utility function to handle login form submit
import { LoginRegisterForm } from "../Utility";

const Login = () => {
  const Navigate = useNavigate();

  /* Login form function starts here */
  const [form] = Form.useForm();
  const [spinner, setSpinner] = useState(false);

  const handleLoginFormSubmit = (values) => {
    LoginRegisterForm(values, setSpinner, form, "login", Navigate);
  };
  /* Login form function ends here */

  return (
    <div className="flex justify-center items-center h-80">
      <Form
        form={form} // Passing the form instance here
        name="normal_login"
        className="login-form p-6 border rounded-lg shadow-lg bg-white"
        initialValues={{
          remember: true,
        }}
        style={{
          minWidth: 350,
        }}
        onFinish={handleLoginFormSubmit}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your Email!",
            },
          ]}
          className="mb-4"
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Email"
            className="p-2 border rounded-md"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your Password!",
            },
          ]}
          className="mb-4"
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
            className="p-2 border rounded-md"
          />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button px-4 w-full"
          >
            Log in
            {spinner ? <LoadingOutlined /> : ""}
          </Button>
          <div className="mt-2 text-center">
            Or{" "}
            <Link to={"/register"} className="text-blue-500">
              register now!
            </Link>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;
