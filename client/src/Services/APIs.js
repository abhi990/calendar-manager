import { commonrequest } from "./ApiCall";
import { BACKEND_URL } from "./Helper";

// User Login
export const loginFunction = async (data) => {
  return await commonrequest("POST", `${BACKEND_URL}/login`, data);
};

// User Register
export const registerFunction = async (data) => {
  return await commonrequest("POST", `${BACKEND_URL}/register`, data);
};

// Add Events
export const addEventFunction = async (data) => {
  return await commonrequest("POST", `${BACKEND_URL}/addEvent`, data);
};

// Get all events in a given month and year for a User
export const allEventsFunction = async (data) => {
  return await commonrequest("POST", `${BACKEND_URL}/allEvents`, data);
};

// Update an event
export const updateEventFunction = async (data) => {
  return await commonrequest("PUT", `${BACKEND_URL}/updateEvent`, data);
};

// Delete an event
export const deleteEventFunction = async (data) => {
  return await commonrequest("DELETE", `${BACKEND_URL}/deleteEvent`, data);
};
